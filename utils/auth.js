const randomatic = require("randomatic");
const { v4: uuid } = require("uuid");
const bcrypt = require("bcryptjs");

require("dotenv").config();

function generateToken() {
  let token = uuid().replace(/-/g, "");
  for (let i = 0; i < token.length; i++) {
    if (Math.floor(Math.random() * 2) == 0) {
      token = token.substr(0, i) + token[i].toUpperCase() + token.substr(i + 1);
    }
  }
  return token + randomatic("aA0", 30);
}

function generateOtp() {
  return randomatic("0", 6);
}

function hashOtp(text) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(text, 12, (error, hash) => {
      //password field?
      if (error) reject(error);
      text = hash;
      resolve(text);
    });
  });
}

function jwtEncapsulation(data) {
  const encryptedMsg = jwt.sign(data, process.env.JWTSECRET);
  return encryptedMsg;
}

function verifyOtp(text, hashed) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(text, hashed, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  });
}

module.exports = {
  generateToken,
  generateOtp,
  hashOtp,
  verifyOtp,
  jwtEncapsulation,
};
