import isEmail from "validator/es/lib/isEmail";
import isNumeric from "validator/es/lib/isNumeric";

export const checkIsEmail = (text) => {
  return isEmail(text);
};
export const checkIfCodeValid = (text) => {
  return isNumeric(text) && text.length === 6;
};
