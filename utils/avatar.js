const randomatic = require("randomatic");
const { v4: uuid } = require("uuid");

export function generateAvatarName() {
  let token = uuid().replace(/-/g, "");
  for (let i = 0; i < token.length; i++) {
    if (Math.floor(Math.random() * 2) == 0) {
      token = token.substr(0, i) + token[i].toUpperCase() + token.substr(i + 1);
    }
  }
  return token + randomatic("aA0", 30);
}

export function decodeBase64Image(dataString) {
  let data = dataString.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
  return data;
}
