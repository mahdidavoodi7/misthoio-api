# How to run the project:

First of all you need to run `yarn` in order to install all the dependencies on your system.
Then you need to run the redis on your system, you can configure the port with `REDIS_PORT` in .env file.
For running the project you also need to connect to a mongoDB database which you can configure that with `MONGO_URI` in .env file.

After all these steps you can run the project with `yarn start` and it will automaticllay run on 3001 port you need to change the `PORT` in .env file.
