import axios from "axios";
import User from "../models/users";
const redis = require("../redis").getConnection();

exports.auth = async (req, res, next) => {
  try {
    const token = req.cookies.token;

    if (!token) return res.status(401).send("Access Token is required");

    redis.get(token, async (err, reply) => {
      if (err) {
        return res.status(500).send({ error: "Something went wrong" });
      }
      if (reply) {
        const user = await User.findOne({
          _id: reply,
        });
        if (!user) {
          res.clearCookie("token");
          return res.status(401).send("Invalid Access Token");
        } else {
          redis.expire(token, 604800);
          req.user = user;
          next();
        }
      } else {
        res.clearCookie("token");
        return res.status(401).send("Invalid Access Token");
      }
    });
  } catch (err) {
    next(err);
  }
};
