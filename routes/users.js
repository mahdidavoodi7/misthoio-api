import express from "express";
import User from "../models/users";

const fs = require("fs");
const path = require("path");

const { auth } = require("../middlewares/auth");

const axios = require("axios");
const router = express.Router();


router.get("/me", auth, async (req, res) => {
  try {
    return res.send({
      email: req.user.email,
    });
  } catch (e) {
    return res
      .status(500)
      .send({ error: "Errore nel recupero utente dal database" });
  }
});

module.exports = router;
