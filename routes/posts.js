import express from "express";
import puppeteer from "puppeteer";
import Post from "../models/posts";

const { auth } = require("../middlewares/auth");

const router = express.Router();

const getContent = async (page, part) => {
  const content = await page.evaluate((part) => {
    let posts = [];

    let allSections = document.querySelectorAll("section")[part];
    let specificSection = allSections.querySelectorAll("h3");

    specificSection.forEach((section) => {
      let title = section.closest("div").querySelector("h3").innerText;
      let image = section.closest("div").querySelector("img");
      if (image) {
        image = image.src;
      }
      let url = section.closest("div").querySelector("a").href;
      let paragraphsHtml = section.closest("div").querySelectorAll("p");
      let tag = null;
      let description = null;
      if (paragraphsHtml.length === 2) {
        tag = paragraphsHtml[0].innerText;
        description = paragraphsHtml[1].innerText;
      } else {
        description = paragraphsHtml[0].innerText;
      }
      posts.push({
        title,
        description,
        tag,
        image,
        url,
      });
    });
    return posts;
  }, part);

  return content;
};
const getSubjectiveContent = async (page) => {
  const content = await page.evaluate(() => {
    let posts = {};

    let allSections = document.querySelectorAll("section")[7];
    let specificSection = allSections.children;

    for (let section of specificSection) {
      let subject = section.querySelector("h3").innerText;
      let titles = section.querySelectorAll("li");
      titles.forEach((title) => {
        let image = title.children[0].querySelector("img");
        if (image) {
          image = image.src;
        }
        let name = title.children[0].querySelector("h3").innerText;
        let url = title.children[0].querySelector("a").href;
        if (!posts[subject]) {
          posts[subject] = [];
        }
        posts[subject].push({
          title: name,
          image,
          url,
        });
      });
    }

    return posts;
  });

  return content;
};
const getSpecificPostContent = async (page) => {
  const content = await page.evaluate(() => {
    let headline = document.querySelector(".article__headline").textContent;
    let subHeadline = document.querySelector(
      ".article__subheadline"
    ).textContent;
    let description = document.querySelector(
      ".article__description"
    ).textContent;
    let sectionHeadline = document.querySelector(
      ".article__section-headline"
    ).textContent;
    let edition = document.querySelector(
      ".article__dateline-datetime"
    ).textContent;
    let writer = document.querySelector(".article__byline");
    if (writer) {
      writer = writer.textContent;
    }
    let image = document.querySelector(".article__lead-image img");
    if (image) {
      image = image.src;
    }
    let body = document.querySelector(".layout-article-body").children;
    let parsedBody = [];
    for (const obj in body) {
      if (obj != 0 && obj != 1 && obj != body.length - 1) {
        parsedBody.push(body[obj].outerHTML);
      }
    }
    parsedBody = parsedBody.join();

    return {
      headline,
      subHeadline,
      description,
      sectionHeadline,
      edition,
      writer,
      image,
      body: parsedBody,
    };
  });

  return content;
};

router.get("/", auth, async (req, res) => {
  try {
    const browser = await puppeteer.launch({
      headless: true,
      args: ["--no-sandbox"],
    });
    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    await page.goto("https://economist.com");

    const resultsSelector = "#new-relic-top-stories";
    await page.waitForSelector(resultsSelector);

    // let topContent = await getTopContent(page);
    let topContent = await getContent(page, 0);
    // let pandemicContent = await getContent(page, 2);
    let theWorldAheadContent = await getContent(page, 2);
    let subjectsContent = await getSubjectiveContent(page);

    res.send({
      topContent,
      // pandemicContent,
      theWorldAheadContent,
      subjectsContent,
    });
    await browser.close();
  } catch (err) {
    console.log(err);
  }
});
router.get("/:id", auth, async (req, res) => {
  try {
    const id = req.params.id;
    if (!id) {
      return res.sendStatus(404);
    }
    let post = await Post.findOne({ _id: id });
    if (!post) {
      return res.sendStatus(404);
    }
    return res.send(post);
  } catch (err) {
    console.log(err);
  }
});
router.post("/", auth, async (req, res) => {
  try {
    const url = req.body.url;
    if (!url) {
      return res.sendStatus(401);
    }
    let post = await Post.findOne({ url });

    let content;
    if (!post) {
      const browser = await puppeteer.launch({
        headless: true,
        args: ["--no-sandbox"],
      });
      const page = await browser.newPage();
      await page.setUserAgent(
        "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36"
      );
      await page.setDefaultNavigationTimeout(0);
      await page.setViewport({ width: 1800, height: 1046 });
      await page.goto(url);

      const resultsSelector = ".layout-article-body";
      await page.waitForSelector(resultsSelector);

      content = await getSpecificPostContent(page);
      post = await Post.create({ ...content, url });
      await browser.close();
    }

    res.send(post._id);
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;
