import express from "express";
import User from "../models/users";
import { checkIsEmail } from "../utils/validations";
import bcrypt from "bcryptjs";

const randomatic = require("randomatic");
const { v4: uuid } = require("uuid");
const redis = require("../redis").getConnection();

function generateToken() {
  let token = uuid().replace(/-/g, "");
  for (let i = 0; i < token.length; i++) {
    if (Math.floor(Math.random() * 2) == 0) {
      token = token.substr(0, i) + token[i].toUpperCase() + token.substr(i + 1);
    }
  }
  return token + randomatic("aA0", 30);
}
function createHash(text) {
  return new Promise((resolve, reject) => {
    bcrypt.hash(text, 12, (error, hash) => {
      //password field?
      if (error) reject(error);
      text = hash;
      resolve(text);
    });
  });
}
function compareHash(text1, text2) {
  return new Promise((resolve, reject) => {
    bcrypt.compare(text1, text2, async (err, result) => {
      //password field?
      if (err) reject(err);
      console.log(result);
      text1 = text2;
      resolve(result);
    });
  });
}

const router = express.Router();
router.post("/", async (req, res) => {
  try {
    let email = req.body.email.toLowerCase();
    let password = req.body.password;
    if (!email || !password) {
      return res.sendStatus(400);
    }
    if (email.length === 0 || password.length < 6) {
      return res.sendStatus(400);
    }
    let user = await User.findOne({
      email: email,
    });
    if (!user) {
      let hashedPass = await createHash(password);
      user = await User.create({
        email: email,
        password: hashedPass,
      });
      const token = generateToken();
      redis.set(token, user._id.toString(), "EX", 604800);
      await res.cookie("token", token, {
        httpOnly: true,
        maxAge: 604800000,
      });

      return res.sendStatus(200);
    } else {
      let auth = await compareHash(password, user.password);
      if (auth) {
        const token = generateToken();
        console.log(token, user._id);
        redis.set(token, user._id.toString(), "EX", 604800);
        await res.cookie("token", token, {
          httpOnly: true,
          maxAge: 604800000,
        });
        return res.sendStatus(200);
      } else {
        return res.sendStatus(401);
      }
    }
  } catch (e) {
    console.log(e);
    return res.status(500).send({ error: "error" });
  }
});

router.post("/login", async (req, res) => {
  try {
    let { email } = req.body;
    if (!email) {
      return res.status(400).send({ error: "error" });
    }
    if (!checkIsEmail(email)) {
      return res.status(400).send({ error: "error" });
    }
    email = email.toLowerCase();

    return res.sendStatus(200);
  } catch (e) {
    console.log(e);
    return res.status(500).send({ error: "error" });
  }
});

module.exports = router;
