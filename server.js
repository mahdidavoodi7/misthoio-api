import express from "express";
import http from "http";
import bodyParser from "body-parser";
import mongoose from "mongoose";
import cors from "cors";
import cookieParser from "cookie-parser";

require("dotenv").config();

const port = process.env.PORT || 3000;
const userRouter = require("./routes/users");
const postRouter = require("./routes/posts");
const authRouter = require("./routes/authentication");

const app = express();

global.CWD = __dirname;

app.use(cors());
app.use(
  bodyParser.json({
    verify: (req, res, buf) => {
      req.rawBody = buf;
    },
    limit: "50mb",
  })
);

app.use(bodyParser.urlencoded({ extended: false, limit: "50mb" }));
app.use(cookieParser());

/* Routers */
app.use("/api/auth", authRouter);
app.use("/api/user", userRouter);
app.use("/api/post", postRouter);

// DB Connection
mongoose
  .connect(process.env.MONGO_URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
  })
  .then(() => console.log("Database succefully connected"))
  .catch((err) => console.log(err));

const httpServer = http.createServer(app);

httpServer.listen(port, function listening() {
  console.log("Server is running on port:", port);
});
