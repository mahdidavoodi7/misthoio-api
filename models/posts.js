const { Schema, model } = require("mongoose");

const postSchema = new Schema({
  headline: {
    type: String,
  },
  subHeadline: {
    type: String,
  },
  description: {
    type: String,
  },
  sectionHeadline: {
    type: String,
  },
  edition: {
    type: String,
  },
  writer: {
    type: String,
  },
  image: {
    type: String,
  },
  body: {
    type: String,
  },
  url: {
    type: String,
  }
});

module.exports = model("Post", postSchema);
