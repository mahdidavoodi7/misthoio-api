const { Schema, model } = require("mongoose");

const userSchema = new Schema({
  // email of the user
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdAt: {
    type: String,
    required: true,
    default: new Date().toISOString(),
  },
});

module.exports = model("User", userSchema);
