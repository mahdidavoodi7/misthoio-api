const redis = require("redis");

class Redis {
  constructor() {
    this.host = "127.0.0.1";
    this.port = process.env.REDIS_PORT || 6379;
    this.connected = false;
    this.client = null;
  }
  getConnection() {
    if (this.connected) return this.client;
    else {
      this.client = redis.createClient(this.port, this.host);
      this.client.on("connect", () => {
        this.connected = true;
      });
      this.client.on("error", function (error) {
        console.error("there is an error running redis");
      });
      return this.client;
    }
  }
}

// This will be a singleton class. After first connection npm will cache this object for whole runtime.
// Every time you will call this getConnection() you will get the same connection back
module.exports = new Redis();
